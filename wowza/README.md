# Wowza Streaming Engine
Wowza Streaming Engine maakt het mogelijk om lokaal of op de cloud gehost, streams te ingesten en te live streamen in meerdere formaten.
![alt text](azure-wowza-settings.png)
