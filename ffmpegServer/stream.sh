#!/bin/bash

while :
do
  ffmpeg -f flv -listen 1 -i rtmp://10.0.1.5:1935/live/app -an -c:v copy -ldash 1 -streaming 1 -use_template 1 -use_timeline 0 -seg_duration 6 -framerate 30 -utc_timing_url https://time.akamai.com/?iso -tune zerolatency -remove_at_exit 1 -f dash /home/thomas/streaming-setups/ffmpegServer/cmaf/live/app.mpd
done

