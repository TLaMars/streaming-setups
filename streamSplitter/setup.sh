#!/bin/bash

# Install updates and then install nginx with rtmp module
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install nginx -y
sudo apt-get install libnginx-mod-rtmp -y

# Echo the nginx.conf into the nginx folder
sudo cat nginx.conf > /etc/nginx/nginx.conf

# Restart nginx
sudo systemctl restart nginx
