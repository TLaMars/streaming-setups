# Streaming Setups
This repo is part of the CMAF streaming setup for the thesis from Thomas Lamars.

## What is in this repo
In this repo there are several different configuration and scripts to setup virtual machines for a CMAF streaming setup in the cloud.
The setup contains three different virtual machines:
- Stream Splitter
- FFMPEG server
- Wowza

All virtual machines are listening on port `1935` so it is required to open this port so the stream can be received. <br>
For the FFMPEG virtual machine it is also required to open port `80` to access the stream.

### Stream Splitter
The stream splitter is required to split the source stream in to multiple RTMP streams.
These streams can than be sent to the FFMPEG and Wowza virtual machines.

The splitter is listening on port 1935 so it is required to open this port so the stream can be received.

To install the streams splitter configuration it is needed to first know the following:
- IP address from the FFMPEG virtual machine
- IP address from the Wowza virtual machine.

These ip address need to be changed in the nginx.conf file. <br>
Change the `<ip-here>` field in the conf file to the ip you want.

After changing the ip addresses it is time to start the configuration. <br>
Make sure you are in the `streamSplitter` folder.
```bash
sudo ./setup.sh
```
You will be asked a few questions during the install. Anwser all of them with yes.

After the installation your stream splitter is up and running.

### FFMPEG 
To create an LL-DASH CMAF stream we need FFMPEG to create such a stream. <br>
To setup FFMPEG run the following command:
``` bash
./setup.sh
```
You will be asked a few questions during the install. Anwser all of them with yes.

After that FFMPEG is installed and the required node server is installed to sent the CMAF chunks. <br>
When running FFMPEG and the node server in the same way as the setup scripts. You are required to keep your terminal open. To prevent this issue these to scripts can be started by using nohup and doing the following:

FFMPEG Stream
``` bash
nohup ./stream.sh &
```

Node Server
``` bash
nohup ./server.sh
```

FFMPEG is accesible from the ip address of the virtual machine and the correct path to the stream files. <br>
This can be different on every system but will most likely by: `/ffmpegServer/cmaf/live/app.mpd`

### Wowza
It is required to have a Wowza Streaming Engine license and a Streamlock file from your Wowza account. <br>
Next to that you need to download the newest Wowza Streaming Engine version and install this on a Windows Server virtual machine.

If the Wowza Streaming Engine is installed, you can copy the `wowza-con.zip`. <br>
It is recommended to replace the streamlock file in the conf file with your streamlock file.

After copying the configuration files in to the Wowza Streaming Engine directory in the Windows Server virtual machine. You need to restart the Wowza Streaming Engine in `localhost:8088` and going to server.
-- EXPAND THIS -- 


